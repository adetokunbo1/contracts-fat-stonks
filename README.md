Repo för OpenAPI kontrakt rörande Fat Stonks

Version: 1.1

You can install the command line CLI using npm i -g @stoplight/prism-cli

To get an overview of all the commands, just do prism help

Du kan starta en prism mock server via deras CLI

genom att ha installerat prism-cli globalt och köra:
`prism mock contractsJSON/Users/fat_stonks_accounts.v1.json`

alternativt:
`prism mock contractsYAML/Users/fat_stonks_accounts.v1.yaml`
